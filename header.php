<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package darkfoliodimensional
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrapper">
  <div class="container">
    <div class="row">
      <header class="col-lg-12">
        <div class="clearfix"></div>
        <div class="dfmmain-nav">
          <div class="row">
              <div class="col-md-5 col-lg-5">
                <div class="navbar-header">
                <!-- Logo -->
                <?php
                if(has_custom_logo())
                {
                // Display the Custom Logo
                the_custom_logo();
                }
                 else { ?>
                <a class="navbar-brand" href="<?php echo esc_url(home_url( '/' )); ?>">
                </a>      
                <?php } ?>
                <!-- Logo -->
                </div>
              </div>
          </div>
        </div>
      </header>
    </div>
<!-- #masthead -->
