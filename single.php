<!-- =========================
     Page Breadcrumb   
============================== -->
<?php get_header(); ?>
<div class="clearfix"></div>
<!-- =========================
     Page Content Section      
============================== -->
 <main id="content">

  <?php get_template_part('navbar','');?>

      <div class="row">
        <div class="<?php echo ( !is_active_sidebar( 'sidebar-1' ) ? 'col-lg-12' :'col-md-9 col-lg-9' ); ?>">
		      <?php if(have_posts())
		        {
		      while(have_posts()) { the_post(); ?>
          <div class="single-content">
            <hr style="border: 1px solid #D5D5D5;background-color:#D5D5D5;">
            <div class="dfmblog-post-box">

              <article class="small">

                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

                <div class="dfmblog-category post-meta-data">

                  <span><?php echo get_the_date( 'F j, Y' ); ?></span>
        
                  | Posted in<a href="#">
                    <?php   $cat_list = get_the_category_list();
                    if(!empty($cat_list)) { ?>
                    <?php the_category(', '); ?>
                  </a>
                  <?php } ?>
                </div>
                <?php the_content(); ?>
              </article>
              <?php wp_link_pages( array( 'before' => '<div class="link single-page-break">' . __( 'Pages:', 'darkfoliodimensional' ), 'after' => '</div>' ) ); ?>
              <div class="social-content">
                <h2>Enjoy the post?</h2>
                <p>Consider sharing it on the following social bookmarking sites.</p>
                <ul class="dfmsocial">
                  <a href="<?php echo 'http://www.facebook.com/share.php?u=';the_permalink(); ?>"> <li><span class="icon-soci"> <i class="fa fa-facebook"></i></span></li> </a>
                  
                  <a href="<?php echo 'https://www.linkedin.com/shareArticle?url=';the_permalink(); ?>" > <li><span class="icon-soci"><i class="fa fa-linkedin"></i></span></li> </a>
                  
                  <a href="<?php echo 'http://twitter.com/home?status=';the_permalink(); ?>" > <li><span class="icon-soci"><i class="fa fa-twitter"></i></span></li> </a>
                  
                  <a href="<?php echo 'https://plus.google.com/share?url=';the_permalink(); ?>" > <li><span class="icon-soci"><i class="fa fa-google-plus"></i></a></span></li> </a>
                </ul>
              </div>
          </div>
		      <?php } ?>
		      <?php } ?>
         <?php comments_template('',true); ?>
        </div>
      </div>
      <div class="col-md-3 col-lg-3">
      <?php get_sidebar(); ?>
      </div>
    </div>
</main>
<?php get_footer(); ?>