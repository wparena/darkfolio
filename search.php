<?php
/**
 * The template for displaying search results pages.
 *
 * @package darkfoliodimensional
 */

get_header(); 
?>
<div class="clearfix"></div>
<main id="content">
	<?php get_template_part('navbar','');?>
    <div class="row">
      <div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-lg-12"; } else { echo "col-md-9 col-lg-9"; } ?>">
      	<div class="search-content-area">
	        <?php 
			global $i;
			if ( have_posts() ) : ?>
			<h2><?php printf( __( "Search Results for: %s", 'darkfoliodimensional' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
			<br>
			<?php while ( have_posts() ) : the_post();  
			 get_template_part('content','');
			 endwhile; else : ?>
			<h2><?php _e('Not Found','darkfoliodimensional'); ?></h2>
			<div class="">
			<p><?php _e('Sorry, Do Not match.','darkfoliodimensional' ); ?>
			</p>
			<?php get_search_form(); ?>
			</div><!-- .blog_con_mn -->
			<?php endif; ?>
		</div>
      </div>
	  <aside class="col-md-3 col-lg-3">
        <?php get_sidebar(); ?>
      </aside>
    </div>
</main>
<?php
get_footer();
?>