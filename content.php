<?php 
/**
 * The template for displaying the content.
 * @package darkfoliodimensional
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="dfmblog-post-box">	
		<?php
			if (('post' === get_post_type() && !post_password_required()) && (comments_open() || get_comments_number())) :
				if ((has_post_thumbnail()) || (!has_post_thumbnail() && !is_single())) :
					?>
					<div class="comments-count">
						<a href="<?php esc_url(comments_link()); ?>">
							<?php
							echo number_format_i18n(get_comments_number());
							?>
						</a>
					</div>
					<?php
				endif;
			endif;
		?>

		<?php if(has_post_thumbnail()): ?>
			<a href="<?php the_permalink(); ?>" class="dfmblog-thumb col-lg-5 col-md-5 col-xs-12">
				<?php if(has_post_thumbnail()): ?>
				<?php $defalt_arg =array('class' => "img-responsive"); ?>
				<?php the_post_thumbnail('', $defalt_arg); ?>
				<?php endif; ?>
			</a>
		<?php endif; ?>

		<article class="small">
			<h2><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
			  <?php the_title(); ?>
			  </a>
			</h2>
			<div class="dfmblog-category post-meta-data"> 
				
				<span><?php echo get_the_date( 'F j, Y' ); ?></span>

				| Posted in<a href="#">
				  <?php   $cat_list = get_the_category_list();
				  if(!empty($cat_list)) { ?>
				  <?php the_category(', '); ?>
				</a>
				<?php } ?>
				
			</div>
			<p>
				<?php
					echo get_the_excerpt();
				?>
			</p>
				<?php wp_link_pages( array( 'before' => '<div class="link">' . __( 'Pages:', 'darkfoliodimensional' ), 'after' => '</div>' ) ); ?>
		<hr style="border-color: grey;margin-top: 40px;">
		</article>
	</div>
</div>
